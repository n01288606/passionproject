﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;//Used for Table Mapping

namespace passionproject.Models
{
    [Table("Definingterms")]
    public class Definingterms
    {

        //definingterms ID
        [Key]
        public int DefiningtermsID { get; set; }

        public int WordID { get; set; }

        //definingterms
        public string Terms { get; set; } 


        //one word associated with multiple terms
        
        public virtual Word Word { get; set; }


    }
}