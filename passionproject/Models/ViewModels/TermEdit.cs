﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace passionproject.Models.ViewModels
{

    public class TermEdit
    {
        //EMpty Constructor
        public TermEdit()
        {

        }
        //what information do I need in order to present DefiningTerm Create
        //need list of all words
        public virtual Definingterms Definingterms { get; set; }
        public virtual IEnumerable<Word> Words { get; set; }
    }
}