﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace passionproject.Models
{
    public class TermsContext  : DbContext
    {

        public TermsContext()
        {

        }

        public DbSet<Definingterms> Definingterms { get; set; }
        public DbSet<Word>Words { get; set; }
    }
}