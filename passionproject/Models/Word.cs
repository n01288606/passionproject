﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;//Used for Table Mapping

namespace passionproject.Models
{
    [Table ("Words")]
    public class Word
    {
        //word ID
        [Key]
        public int WordID { get; set; }

        //word
        public string WordTitle {get; set;}

        //country??
        public string Country { get; set; }

        //rating
        public int Rating { get; set; }


        /*There is one word to many defining terms, for example, the word Pickup will be defined by truck and ute. */
        //defining terms
        public virtual ICollection<Definingterms> Definingterms { get; set; }
    }
}