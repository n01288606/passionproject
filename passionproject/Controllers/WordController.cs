﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Net;
using System.Web;
using System.Web.Mvc;
using passionproject.Models;
using System.Diagnostics;

namespace passionproject.Controllers
{
    public class WordController : Controller
    {
        private TermsContext db = new TermsContext();
       
        //are all things not working? No, this one runs
        public ActionResult List()
        {
            return View(db.Words.ToList()); 
        }

        //This isn't working as far as I know, now it's displaying
        public ActionResult NewWord()
        {
            return View();
        }
        


      
        //Works??
        [HttpPost]
        public ActionResult NewWord(string WordTitle_New, string Country_New, string Rating_New )
        {
            //print out the results of the variables
            Debug.WriteLine(Rating_New+Country_New+ WordTitle_New);

            //make an sql query insert into words () values () 
            string query = "insert into words ( WordTitle, Country, Rating) values( @WordTitle, @Country, @Rating)";
            //put the variables from the argument as sql parameters
            SqlParameter[] Wordparams = new SqlParameter[3];
            Wordparams[0] = new SqlParameter("@WordTitle", WordTitle_New);
            Wordparams[1] = new SqlParameter("@Country", Country_New);
            Wordparams[2] = new SqlParameter("@Rating", Rating_New);
            //then run your datbase command  
            db.Database.ExecuteSqlCommand(query, Wordparams);



            return RedirectToAction("List");
        }

    
     
        //=======

        [HttpGet]
        public ActionResult EditWord(int id)
        {
           // Debug.WriteLine("I am in the edit get function");
            //Since we are going to retrieve just one record, we need not create a list, instead store the retrieved results in one object
            Word wordObj = db.Words.Find(id);

            return View(wordObj);
        }
        
        [HttpPost]
        public ActionResult EditWord(int id, string WordTitle_New, string Country_New, int Rating_New)
        
        {
            //Debug.WriteLine(Rating_New + Country_New + WordTitle_New);

            if ((id == null) || (db.Words.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "update words set WordTitle=@WordTitle, Country=@Country, Rating=@Rating where WordID=@id";
            SqlParameter[] Wordparams = new SqlParameter[3];
            Wordparams[0] = new SqlParameter("@WordTitle", WordTitle_New);
            Wordparams[1] = new SqlParameter("@Country", Country_New);
            Wordparams[2] = new SqlParameter("@Rating", Rating_New);
            
            db.Database.ExecuteSqlCommand(query, Wordparams);



            return RedirectToAction("ShowWord/" + id);
        }
        
        public ActionResult Showword(int? id)
        {
            //Debug.WriteLine("This is my show function");

            if ((id == null) || (db.Words.Find(id) == null))
            {
                return HttpNotFound();
            }

            string query = "Select * from words where WordID=@id";
            SqlParameter[] Wordparams = new SqlParameter[1];
            Wordparams[0] = new SqlParameter("@id", id);


            Word myword = db.Words.SqlQuery(query, Wordparams).FirstOrDefault();

            return View(myword);
        }

        
        public ActionResult Delete (int? id)
        {
            if ((id == null) || (db.Words.Find(id) == null))
            {
                return HttpNotFound();

            }

            string Delquery = "delete from words where WordID in (select WordID from Words where Word_WordID=@id)";
            SqlParameter Wordparams = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(Delquery, Wordparams);


            Delquery = "delete from pages where word_WordID=@id";
            Wordparams = new SqlParameter("@id", id);
                db.Database.ExecuteSqlCommand(Delquery, Wordparams);


            Delquery = "delete from Blogs where wordid=@id";
            Wordparams = new SqlParameter("@id", id);
                db.Database.ExecuteSqlCommand(Delquery, Wordparams);
                return RedirectToAction("List");


        }

                
    }
}