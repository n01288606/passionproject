﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Net;
using System.Web;
using System.Web.Mvc;
using passionproject.Models.ViewModels;
using passionproject.Models;
using System.Diagnostics;

namespace passionproject.Controllers
{
    public class DefiningtermsController : Controller
    {
        private TermsContext db = new TermsContext();

        public ActionResult List()
        {
            return View(db.Definingterms.ToList());
        }


        public ActionResult Create()
        {
            //we need to pass information about all words so that we can populate the dropdownlist

            TermEdit mytermedit = new TermEdit();
            mytermedit.Words = db.Words.ToList();
            //could also set mytermedit.Term

            return View(mytermedit);
        }



        [HttpPost]
        public ActionResult Create(string Term_New, int wordid)
        {
            Debug.WriteLine(Term_New);
            //make an sql query insert into words () values () 
            string query = "insert into Definingterms (Terms, WordID) values(@Terms, @wid)"; 
            //put the variables from the argument as sql parameters
            SqlParameter[] Termparams = new SqlParameter[2];
            
            Termparams[0] = new SqlParameter("@Terms", Term_New);
            Termparams[1] = new SqlParameter("@wid", wordid);
           
            //then run your datbase command  
            db.Database.ExecuteSqlCommand(query, Termparams);



            return RedirectToAction("List");
        }

        
        [HttpGet]
        public ActionResult Edit(int id)
        {
            
            TermEdit TermEditview = new TermEdit();
            TermEditview.Words = db.Words.ToList();

            TermEditview.Definingterms = db.Definingterms.Find(id);

            return View(TermEditview);

            //No idea if I did this correctly -- yes

        }
        

        /*
        [HttpPost]
        public ActionResult Edit(int Definingtermsid, int WordID, string Terms)
       

        {
            if ((WordID == null) || (db.Words.Find(WordID) == null))
            {
                return HttpNotFound();
            }
            string query = "update words set Word=@WordID, Terms=@terms where WordID=@id";
           
            SqlParameter[] Termparams = new SqlParameter[3];
            Termparams[0] = new SqlParameter("@wordID", WordID);
            Termparams[1] = new SqlParameter("@Terms", Terms);

            //then run your datbase command  
            db.Database.ExecuteSqlCommand(query, Termparams);



            return RedirectToAction("Show/" + WordID);
        }

        public ActionResult Show(int? id)
        {
            if ((id == null) || (db.Words.Find(id) == null))
            {
                return HttpNotFound();
            }

            string query = "Select * from definingterms where definingterms=@id";
            SqlParameter[] Termparams = new SqlParameter[1];
            Termparams[0] = new SqlParameter("@id", id);

            
            Word myword = db.Words.SqlQuery(query, Termparams).FirstOrDefault();

            return View(myword);
        }
        public ActionResult Delete(int? id)
        {
            if ((id == null) || (db.Words.Find(id) == null))
            {
                return HttpNotFound();

            }

            string Delquery = "delete from words where WordID in (select WordID from Words where Word_WordID=@id)";
            SqlParameter myparams = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(Delquery, myparams);


            Delquery = "delete from pages where blog_BlogID=@id";
            myparams = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(Delquery, myparams);


            Delquery = "delete from Blogs where blogid=@id";
            myparams = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(Delquery, myparams);
            return RedirectToAction("List");


        }*/


    }
}